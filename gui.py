import argparse
import asyncio
import multiprocessing
import os
import platform
import subprocess
import sys
import threading
import tkinter as tk
import webbrowser
from queue import Empty
from tkinter import ttk
from tkinter.font import BOLD, Font
from typing import Any, Callable, List

from PIL import Image, ImageDraw, ImageTk

from rpc import (_DEFAULT, _LOCALES, _OPGG_LIVE_GAME, _PLATFORM_CONVERSION,
                 LoLPresence, RPCButton, RPCPayload, Template,
                 convert_platform_for_url, get_config, start_connector)

VERSION = "0.1.3"


class GUI():
    def __init__(self, root: tk.Tk) -> None:
        self.root = root
        self.config = get_config(self.root)

        self._version = VERSION
        self._populate_root()

        self.root.protocol("WM_DELETE_WINDOW", self.shutdown)
        self._queue = multiprocessing.Queue()
        self._lcu_queue = multiprocessing.Queue()
        self._loop = asyncio.get_event_loop_policy().new_event_loop()
        self._t: threading.Thread = None
        self._thread_stop_flag = False

    def _populate_root(self) -> None:
        self.settings_lf = ttk.LabelFrame(
            master=self.root,
            labelanchor="nw",
            text="Settings",
            width=600,
            height=380,
        )
        self.settings_lf.columnconfigure(0, weight=1)
        self.settings_lf.grid_propagate(False)
        self.settings_lf.grid(column=0, row=0, padx=10, pady=5)
        self._populate_field_settings()
        self._populate_optional_settings()
        self._populate_variable_description()
        self._populate_preview()

        self.metadata = ttk.Label(
            master=self.root,
            text=f"Version {self._version} | © Marwynn Somridhivej (2022 - Present)",
            width=1200,
        )
        self.metadata.grid(column=0, row=3, padx=10, columnspan=2)

        self._bind_all()

    def _populate_field_settings(self) -> None:
        entry_width = 70
        button_width = 14

        # State labelframe
        self.settings_state_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="State",
            width=600,
            height=50,
        )
        self.settings_state_lf.columnconfigure(0, weight=1)
        self.settings_state_lf.columnconfigure(1, weight=1)
        self.settings_state_lf.grid_propagate(False)
        self.settings_state_lf.grid(column=0, row=0, padx=5, pady=5)

        self.settings_state_entry = ttk.Entry(
            master=self.settings_state_lf,
            textvariable=self.config.state,
            width=entry_width,
        )
        self.settings_state_entry.grid(column=0, row=0)

        self.settings_state_button = ttk.Button(
            master=self.settings_state_lf,
            text="Use Default",
            command=self._use_default("state"),
            width=button_width,
        )
        self.settings_state_button.grid(column=1, row=0)

        # Details labelframe
        self.settings_details_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="Details",
            width=600,
            height=50,
        )
        self.settings_details_lf.columnconfigure(0, weight=1)
        self.settings_details_lf.columnconfigure(1, weight=1)
        self.settings_details_lf.grid_propagate(False)
        self.settings_details_lf.grid(column=0, row=1, padx=5, pady=5)

        self.settings_details_entry = ttk.Entry(
            master=self.settings_details_lf,
            textvariable=self.config.details,
            width=entry_width,
        )
        self.settings_details_entry.grid(column=0, row=0)

        self.settings_details_button = ttk.Button(
            master=self.settings_details_lf,
            text="Use Default",
            command=self._use_default("details"),
            width=button_width,
        )
        self.settings_details_button.grid(column=1, row=0)

        # Large image key
        self.settings_large_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="Large Image Key",
            width=600,
            height=50,
        )
        self.settings_large_lf.columnconfigure(0, weight=1)
        self.settings_large_lf.columnconfigure(1, weight=1)
        self.settings_large_lf.grid_propagate(False)
        self.settings_large_lf.grid(column=0, row=2, padx=5, pady=5)

        self.settings_large_key = ttk.Entry(
            master=self.settings_large_lf,
            width=entry_width,
        )
        self.settings_large_key.insert(tk.END, "{champion}")
        self.settings_large_key.config(state="disabled")
        self.settings_large_key.grid(column=0, row=0)

        self.settings_large_button = ttk.Button(
            master=self.settings_large_lf,
            text="Use Default",
            width=button_width,
            state="disabled",
        )
        self.settings_large_button.grid(column=1, row=0)

        # Large image text
        self.settings_large_text_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="Large Image Text",
            width=600,
            height=50,
        )
        self.settings_large_text_lf.columnconfigure(0, weight=1)
        self.settings_large_text_lf.columnconfigure(1, weight=1)
        self.settings_large_text_lf.grid_propagate(False)
        self.settings_large_text_lf.grid(column=0, row=3, padx=5, pady=5)

        self.settings_large_text = ttk.Entry(
            master=self.settings_large_text_lf,
            width=entry_width,
        )
        self.settings_large_text.insert(tk.END, "{champion} ({skin})")
        self.settings_large_text.config(state="disabled")
        self.settings_large_text.grid(column=0, row=0)

        self.settings_large_text_button = ttk.Button(
            master=self.settings_large_text_lf,
            text="Use Default",
            width=button_width,
            state="disabled",
        )
        self.settings_large_text_button.grid(column=1, row=0)

        # Small image key
        self.settings_small_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="Small Image Key",
            width=600,
            height=50
        )
        self.settings_small_lf.columnconfigure(0, weight=1)
        self.settings_small_lf.columnconfigure(1, weight=1)
        self.settings_small_lf.grid_propagate(False)
        self.settings_small_lf.grid(column=0, row=4, padx=5, pady=5)

        self.settings_small_key = ttk.Entry(
            master=self.settings_small_lf,
            textvariable=self.config.small_image,
            width=entry_width,
        )
        self.settings_small_key.grid(column=0, row=0)

        self.settings_small_button = ttk.Button(
            master=self.settings_small_lf,
            text="Use Default",
            command=self._use_default("small_image"),
            width=button_width,
        )
        self.settings_small_button.grid(column=1, row=0)

        # Small image text
        self.settings_small_text_lf = ttk.LabelFrame(
            master=self.settings_lf,
            labelanchor="nw",
            text="Small Image Text",
            width=600,
            height=50,
        )
        self.settings_small_text_lf.columnconfigure(0, weight=1)
        self.settings_small_text_lf.columnconfigure(1, weight=1)
        self.settings_small_text_lf.grid_propagate(False)
        self.settings_small_text_lf.grid(column=0, row=5, padx=5, pady=5)

        self.settings_small_text = ttk.Entry(
            master=self.settings_small_text_lf,
            textvariable=self.config.small_text,
            width=entry_width,
        )
        self.settings_small_text.config(state="disabled")
        self.settings_small_text.grid(column=0, row=0)

        self.settings_small_text_button = ttk.Button(
            master=self.settings_small_text_lf,
            text="Use Default",
            command=self._use_default("small_text"),
            width=button_width,
            state="disabled",
        )
        self.settings_small_text_button.grid(column=1, row=0)

    def _populate_optional_settings(self) -> None:
        self.optional_f = ttk.Frame(
            master=self.root,
            width=600,
            height=160,
        )
        self.optional_f.columnconfigure(0, weight=1)
        self.optional_f.columnconfigure(1, weight=1)
        self.optional_f.grid_propagate(False)
        self.optional_f.grid(column=0, row=1)

        self.optional_lf = ttk.LabelFrame(
            master=self.optional_f,
            labelanchor="nw",
            text="Optional Settings",
            width=300,
            height=160,
        )
        self.optional_lf.columnconfigure(0, weight=1)
        self.optional_lf.rowconfigure(0, weight=1)
        self.optional_lf.rowconfigure(1, weight=1)
        self.optional_lf.rowconfigure(2, weight=1)
        self.optional_lf.grid_propagate(False)
        self.optional_lf.grid(column=0, row=0, padx=10)

        for button in self.config.buttons:
            if button.label == "{livegame}":
                opgg_exists = True
                break
        else:
            opgg_exists = False
        self._show_opgg = tk.BooleanVar(master=self.root, value=opgg_exists)

        def set_opgg() -> None:
            if self._show_opgg.get():
                self.config.buttons = [
                    RPCButton({
                        "label": Template.LIVE_GAME,
                        "url": Template.LIVE_GAME,
                    })
                ]
                self.platform_dropdown.config(state="normal")
            else:
                self.config.buttons.clear()
                self.platform_dropdown.config(state="disabled")
            self.config.save()
            self._update_preview()
            self._update_opgg_button()

        self.opgg_checkbox = ttk.Checkbutton(
            master=self.optional_lf,
            width=28,
            text="Show OPGG Live Game Button",
            command=set_opgg,
            variable=self._show_opgg,
        )
        self.opgg_checkbox.grid(column=0, row=0, padx=5, pady=5)

        def convert_platform(*args, from_key: bool = False) -> None:
            if from_key:
                for (name, key) in _PLATFORM_CONVERSION.items():
                    if key == self.config.platform.get():
                        return name
            else:
                self.config.platform.set(_PLATFORM_CONVERSION[self._platform.get()])
                self.config.save()
                self._update_preview()

        self._platform = tk.StringVar(master=root, value=convert_platform(from_key=True))
        platforms = [platform for platform in _PLATFORM_CONVERSION.keys()]

        self.platform_f = ttk.Frame(
            master=self.optional_lf,
            width=260,
            height=50,
        )
        self.platform_f.rowconfigure(0, weight=1)
        self.platform_f.rowconfigure(1, weight=1)
        self.platform_f.columnconfigure(0, weight=1)
        self.platform_f.grid_propagate(False)
        self.platform_f.grid(column=0, row=1, padx=5, pady=5)

        self.platform_label = ttk.Label(
            master=self.platform_f,
            text="Select Your Server",
        )
        self.platform_label.grid(column=0, row=0)

        self.platform_dropdown = ttk.OptionMenu(
            self.platform_f,            # master
            self._platform,             # variable
            self._platform.get(),       # default
            *platforms,
            command=convert_platform,
        )
        self.platform_dropdown.config(state="normal" if self.config.buttons else "disabled")
        self.platform_dropdown.grid(column=0, row=1)

        self.locale_f = ttk.Frame(
            master=self.optional_lf,
            width=260,
            height=50,
        )
        self.locale_f.rowconfigure(0, weight=1)
        self.locale_f.rowconfigure(1, weight=1)
        self.locale_f.columnconfigure(0, weight=1)
        self.locale_f.grid_propagate(False)
        self.locale_f.grid(column=0, row=2, padx=5, pady=5)

        def convert_locale(*args, from_key: bool = False) -> None:
            if from_key:
                return _LOCALES[self.config.locale.get()]
            for (locale, text) in _LOCALES.items():
                if text == self._locale.get():
                    self.config.locale.set(locale)
                    self.config.save()
                    self._update_preview()
            return

        self._locale = tk.StringVar(master=root, value=convert_locale(from_key=True))
        locales = [locale for locale in _LOCALES.values()]

        self.locale_label = ttk.Label(
            master=self.locale_f,
            text="Select Rich Presence Language",
        )
        self.locale_label.grid(column=0, row=0)

        self.locale_dropdown = ttk.OptionMenu(
            self.locale_f,
            self._locale,
            self._locale.get(),
            *locales,
            command=convert_locale,
        )
        self.locale_dropdown.grid(column=0, row=1)

        self.runtime_lf = ttk.LabelFrame(
            master=self.optional_f,
            labelanchor="nw",
            text="Rich Presence Controls",
            width=300,
            height=160,
        )
        self.runtime_lf.columnconfigure(0, weight=1)
        self.runtime_lf.rowconfigure(0, weight=1)
        self.runtime_lf.rowconfigure(1, weight=1)
        self.runtime_lf.grid_propagate(False)
        self.runtime_lf.grid(column=1, row=0, padx=10)

        self.start_button = ttk.Button(
            master=self.runtime_lf,
            text="Start Rich Presence",
            width=24,
            command=self.start_rpc,
        )
        self.start_button.grid(column=0, row=0, pady=10)

        self.stop_button = ttk.Button(
            master=self.runtime_lf,
            text="Stop Rich Presence",
            width=24,
            command=self.stop_rpc,
            state="disabled",
        )
        self.stop_button.grid(column=0, row=1, pady=10)

    def _populate_variable_description(self) -> None:
        self.variables_lf = ttk.LabelFrame(
            master=self.root,
            labelanchor="nw",
            text="Variables Info",
            width=600,
            height=200,
        )
        self.variables_lf.columnconfigure(0, weight=1)
        self.variables_lf.columnconfigure(1, weight=1)
        self.variables_lf.rowconfigure(0, weight=1)
        self.variables_lf.rowconfigure(1, weight=1)
        self.variables_lf.rowconfigure(2, weight=1)
        self.variables_lf.rowconfigure(3, weight=1)
        self.variables_lf.grid_propagate(False)
        self.variables_lf.grid(column=0, row=2, padx=10, pady=5)

        self.variables_desc_label = ttk.Label(
            master=self.variables_lf,
            text="Variables are placeholder strings formatted in a specific way "
            "that will be recognised by Enhanced LoL Rich Presence and be "
            "automatically replaced with data provided by League of Legends. "
            "However, not every field supports every variable. Listed below are "
            "the valid field-variable combinations this application supports:",
            wraplength=580
        )
        self.variables_desc_label.grid(column=0, row=0, padx=5, pady=3, sticky=tk.W, columnspan=2)

        variables = {
            "state": [Template.KDA, Template.CS, Template.CSPM, Template.VISION_SCORE, Template.KP],
            "details": [Template.MAP, Template.GAMEMODE],
            "small image key": [Template.LANE, Template.MASTERY, Template.RANK, Template.RANK_SOLO, Template.RANK_FLEX]
        }

        _iter = 1
        for (field, variables) in variables.items():
            ttk.Label(
                master=self.variables_lf,
                text=f"{field.title()}: {', '.join(variables)}",
                wraplength=500,
            ).grid(column=0, row=_iter, padx=5, pady=3, sticky=tk.W, rowspan=1)
            _iter += 1

        self.variables_desc_button = ttk.Button(
            master=self.variables_lf,
            text="See Variable Conversions",
            width=24,
            command=self.create_variable_popup,
        )
        self.variables_desc_button.grid(column=1, row=2, padx=5, pady=5, columnspan=1)

    def create_variable_popup(self) -> None:
        self.variables_desc_button.config(state="disabled")
        popup = tk.Toplevel(
            master=self.root,
            width=600,
            height=400,
        )
        popup.title("Variable Conversions")
        if platform.system() == "Windows":
            icon = os.path.abspath("./icons/icon.ico")
        else:
            icon = os.path.abspath("./icons/icon.icns")
        popup.iconbitmap(icon)
        popup.resizable(False, False)

        def _handle_close():
            popup.destroy()
            self.variables_desc_button.config(state="normal")

        popup.protocol("WM_DELETE_WINDOW", _handle_close)

        conversion = {
            Template.DEFAULT: "The default value displayed by League's internal RPC implementation",
            Template.KDA: "Your KDA displayed as kills/deaths/assists",
            Template.CS: "Your creep score",
            Template.CSPM: "Your creep score per minute rounded to 2 decimal places",
            Template.VISION_SCORE: "Your vision score rounded to 1 decimal place",
            Template.KP: "Your kill participation, displayed as a percentage "
            "rounded to 1 decimal place",
            Template.MAP: "The name of the map the game is being played on",
            Template.GAMEMODE: "The name of the current queue the game is played on",
            Template.LANE: "Your lane assignment, if applicable",
            Template.MASTERY: "Your mastery level displayed as the badge in "
            "the small image field, and mastery level and mastery points upon "
            "hovering your mouse over the small image",
            Template.RANK: "Your highest rank out of all ranked queues",
            Template.RANK_SOLO: "Your ranked solo/duo information displayed as "
            "your current tier's icon in the small image field, and the rank tier, "
            "division, LP, wins, losses, and winrate upon hovering your mouse "
            "over the small image",
            Template.RANK_FLEX: "Same as above, but for ranked flex",
        }

        for index, (variable, value) in enumerate(conversion.items()):
            ttk.Label(
                master=popup,
                wraplength=580,
                text=f"{variable}: {value}"
            ).grid(column=0, row=index, padx=5, pady=5, sticky=tk.W)

    def _populate_preview(self) -> None:
        self.preview_lf = ttk.LabelFrame(
            master=self.root,
            labelanchor="nw",
            text="Rich Presence Preview",
            width=600,
            height=760,
        )
        self.preview_lf.columnconfigure(0, weight=1)
        self.preview_lf.rowconfigure(0, weight=1)
        self.preview_lf.grid_propagate(False)
        self.preview_lf.grid(column=1, row=0, padx=10, pady=5, rowspan=3)

        self.preview_f = ttk.Frame(
            master=self.preview_lf,
            width=400,
            height=270,
        )
        self.preview_f.columnconfigure(0, weight=1)
        self.preview_f.grid_propagate(False)
        self.preview_f.grid(column=0, row=0, padx=5, pady=5)

        self.preview_playing_a_game = ttk.Label(
            master=self.preview_f,
            text="PLAYING A GAME",
            font=Font(weight=BOLD, size=11)
        )
        self.preview_playing_a_game.grid(column=0, row=0, padx=10, pady=5, sticky=tk.W)

        self.preview_info_f = ttk.Frame(
            master=self.preview_f,
            width=400,
            height=110,
        )
        self.preview_info_f.columnconfigure(0, weight=1)
        self.preview_info_f.columnconfigure(1, weight=3)
        self.preview_info_f.grid_propagate(False)
        self.preview_info_f.grid(column=0, row=1, padx=10, pady=5)

        self.preview_info_text_f = ttk.Frame(
            master=self.preview_info_f,
            width=300,
            height=100,
        )
        self.preview_info_text_f.columnconfigure(0, weight=1)
        self.preview_info_text_f.rowconfigure(0, weight=1)
        self.preview_info_text_f.rowconfigure(1, weight=1)
        self.preview_info_text_f.rowconfigure(2, weight=1)
        self.preview_info_text_f.rowconfigure(3, weight=1)
        self.preview_info_text_f.grid_propagate(False)
        self.preview_info_text_f.grid(column=1, row=0, padx=2, pady=5)

        self._update_preview(first_time=True)

        def open_opgg() -> None:
            webbrowser.open_new_tab(
                _OPGG_LIVE_GAME.format(
                    region=convert_platform_for_url("KR"),
                    summoner_name="hide on bush",
                )
            )

        self.preview_opgg_button = ttk.Button(
            master=self.preview_f,
            text="View Game Info",
            width=40,
            command=open_opgg,
        )
        self.preview_opgg_button.grid(column=0, row=2, padx=10, pady=5, ipadx=60, ipady=10)

        self._update_opgg_button()

    def _populate_canvas(self) -> None:
        self.preview_info_image_canvas = tk.Canvas(
            master=self.preview_info_f,
            width=100,
            height=92,
        )
        self.preview_info_image_canvas.grid_propagate(False)
        self.preview_info_image_canvas.grid(column=0, row=0, padx=2, pady=5)

        image = Image.new("RGBA", (600, 600), color=(0, 0, 0, 0))
        image.paste(Image.open("./assets/champions/Yone.png", "r"), box=(0, 0))

        if self.config.small_image.get() == Template.LANE:
            _image = "./assets/lanes/middle.png"
        elif self.config.small_image.get() == Template.MASTERY:
            _image = "./assets/mastery/m7.png"
        elif self.config.small_image.get() == Template.RANK or self.config.small_image.get() == Template.RANK_SOLO:
            _image = "./assets/rank/diamond.png"
        elif self.config.small_image.get() == Template.RANK_FLEX:
            _image = "./assets/rank/platinum.png"
        else:
            _image = None

        if _image is not None:
            _template = Image.new("RGBA", (512, 512), color=(0, 0, 0, 0))
            _small_image = Image.open(_image, "r")
            if _small_image.width != 512:
                _small_image = _small_image.resize((512, 512), resample=Image.Resampling.LANCZOS)
            draw = ImageDraw.ImageDraw(_template)
            draw.arc([(0, 0), (512, 512)], 0, 360, fill="black", width=256)
            _template.paste(_small_image, mask=_small_image)

            _template = _template.resize((128, 128), resample=Image.Resampling.LANCZOS)
            image.paste(im=_template, box=(416, 416), mask=_template)

        self.preview_image = ImageTk.PhotoImage(
            image=image.resize((100, 100), resample=Image.Resampling.LANCZOS)
        )
        self.preview_info_image_canvas.create_image(0, 0, anchor=tk.NW, image=self.preview_image)

    def _update_canvas(self, first_time: bool = False) -> None:
        if not first_time:
            self.preview_info_image_canvas.destroy()
        self._populate_canvas()

    def _populate_preview_text(self) -> None:
        self.preview_info_text_game = ttk.Label(
            master=self.preview_info_text_f,
            text="League of Legends",
            font=Font(weight=BOLD, size=10)
        )
        self.preview_info_text_game.grid(column=0, row=0, sticky=tk.W)

        self._dss: List[ttk.Label] = []

        for index, text in enumerate([self.config.details.get(), self.config.state.get(), "17:36 elapsed"]):
            if index == 0:
                if text == Template.DEFAULT:
                    text = f"{Template.MAP} ({Template.GAMEMODE})"
                specifics = Template.DETAILS
            elif index == 1:
                if text == Template.DEFAULT:
                    text = "In Game"
                specifics = Template.STATE
            else:
                specifics = None
            label = ttk.Label(
                master=self.preview_info_text_f,
                text=Template.sample(text, specifics=specifics)
            )
            self._dss.append(label)
            label.grid(column=0, row=1 + index, sticky=tk.W)

    def _update_preview(self, first_time: bool = False) -> None:
        if not first_time:
            self.preview_info_text_game.destroy()
            for widget in self._dss:
                widget.destroy()
        self._populate_preview_text()
        self._update_canvas(first_time=first_time)

    def _update_opgg_button(self) -> None:
        if not self._show_opgg.get():
            self.preview_opgg_button.grid_remove()
        else:
            self.preview_opgg_button.grid()

    def _keyup_wrapper(self) -> Callable:
        def wrapper(*args, **kwargs) -> None:
            self.config.save()
            self._update_preview()
        return wrapper

    def _bind_all(self) -> None:
        entries: List[ttk.Entry] = [getattr(self, attr) for attr in dir(self) if type(getattr(self, attr)) == ttk.Entry]
        for entry in entries:
            entry.bind("<KeyRelease>", self._keyup_wrapper())

    def _use_default(self, key: str) -> Callable:
        def wrapper(*args, **kwargs) -> Any:
            default = _DEFAULT[key]
            getattr(self.config, key).set(default)
            self.config.save()
            self._update_preview()
            return
        return wrapper

    def start_rpc(self) -> None:
        self.config.save()
        self._update_preview()
        multiprocessing.Process(
            target=start_connector,
            args=(self._lcu_queue,),
            name="LCU"
        ).start()
        multiprocessing.Process(
            target=start_rpc,
            args=(
                self.config.platform.get(),
                self.config.to_dict(),
                self.config.locale.get(),
                self._queue,
                self._lcu_queue,
            ),
            name="RPC"
        ).start()
        self.start_button.config(state="disabled")
        self.stop_button.config(state="normal")

        def wrapper():
            self._loop.run_until_complete(self._check_queue())

        self._t = threading.Thread(target=wrapper)
        self._t.start()
        return

    async def _check_queue(self) -> None:
        while not self._thread_stop_flag:
            try:
                payload = self._queue.get_nowait()
                if payload["command"] == "rpc_end":
                    self.stop_rpc()
            except Empty:
                await asyncio.sleep(2)
                continue
            else:
                break
        self._thread_stop_flag = False
        return

    def stop_rpc(self) -> None:
        self._thread_stop_flag = True
        self.start_button.config(state="normal")
        self.stop_button.config(state="disabled")
        for process in multiprocessing.active_children():
            process.kill()

    def shutdown(self) -> None:
        self.stop_rpc()
        self.root.destroy()


def start_rpc(_platform: str, data: dict, locale: str,
              gui_queue: multiprocessing.Queue,
              lcu_queue: multiprocessing.Queue,) -> None:
    presence = LoLPresence(
        platform=_platform,
        data=RPCPayload(data),
        locale=locale,
        gui_queue=gui_queue,
        lcu_queue=lcu_queue,
    )
    loop = asyncio.get_event_loop()
    loop.run_until_complete(presence.start())


if __name__ == "__main__":
    multiprocessing.freeze_support()
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--no-update", action="store_true")
    args = parser.parse_args()

    if not args.no_update:
        subprocess.Popen(
            [os.path.abspath(
                "./updater.exe" if platform.system() == "Windows" else "./updater"
            ), "-v", VERSION]
        )
        sys.exit(0)

    # Init root
    root = tk.Tk()
    root.title("Enhanced LoL Rich Presence")

    window_width = 1200
    window_height = 800
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    center_x = int(screen_width / 2 - window_width / 2)
    center_y = int(screen_height / 2 - window_height / 2)

    root.geometry(f"{window_width}x{window_height}+{center_x}+{center_y}")
    root.resizable(False, False)

    root.columnconfigure(0, weight=1)
    root.columnconfigure(1, weight=1)
    root.rowconfigure(0, weight=1)
    root.rowconfigure(1, weight=1)
    root.rowconfigure(2, weight=1)
    root.rowconfigure(3, weight=1)

    # Get icon for appropriate OS
    if platform.system() == "Windows":
        icon = os.path.abspath("./icons/icon.ico")
    else:
        icon = os.path.abspath("./icons/icon.icns")
    root.iconbitmap(icon)

    gui = GUI(root)
    root.mainloop()
