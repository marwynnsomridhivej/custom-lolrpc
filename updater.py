import argparse
import asyncio
import json
import os
import platform
import shutil
import signal
import subprocess
import sys
import threading
import tkinter as tk
import zipfile
from tkinter import ttk
from typing import Optional

import aiohttp
from aiofiles import open as aiopen
from psutil import Process, process_iter


class UpdaterGUI():
    def __init__(self, root: tk.Tk, version: str) -> None:
        self.root = root
        self._version = version
        self._success = True
        self._finished: bool = False
        self._loop = asyncio.get_event_loop()
        self._construct_gui()

    def _construct_gui(self) -> None:
        self.update_status_label = ttk.Label(
            master=self.root,
            text="Updating Enhanced LoL Rich Presence"
        )
        self.update_status_label.grid(column=0, row=0, padx=5, pady=5)

        self.update_status_prog_bar = ttk.Progressbar(
            master=self.root,
            mode="indeterminate",
            length=360,
            maximum=100,
        )
        self.update_status_prog_bar.grid(column=0, row=1, padx=5, pady=5)
        self.update_status_prog_bar.start()

        self.update_status_finish_button = ttk.Button(
            master=self.root,
            text="Close",
            command=self._close_root,
            width=5,
            state="disabled",
        )
        self.update_status_finish_button.grid(column=0, row=2, padx=5, pady=5, ipadx=20, ipady=5)

    def _find_open_rpc(self) -> Optional[Process]:
        for process in process_iter():
            if process.name() in ["Enhanced LoL Rich Presence.exe", "Enhanced LoL Rich Presence"]:
                return process
        return None

    def _close_rpc_process(self) -> None:
        rpc_process = self._find_open_rpc()
        if rpc_process is not None:
            os.kill(rpc_process.pid, signal.SIGTERM)
        return

    def _show_window(self) -> None:
        self.root.protocol("WM_DELETE_WINDOW", self._close_root)
        self.root.after(0, self._gui_check_done)
        self.root.after(0, self.handle_update)
        self.root.mainloop()

    def _close_root(self) -> None:
        print("_close_root called")
        if self._finished:
            self.root.after(20, self.root.destroy)

    def _rmtree(self) -> None:
        for path in ["./tmp", "./backup"]:
            if os.path.exists(path):
                shutil.rmtree(path)

    def _prep(self) -> None:
        self._close_rpc_process()
        self._rmtree()
        os.mkdir("./tmp")
        os.mkdir("./backup")
        for path in [
            "assets",
            "icons",
            "lang",
            "config.json",
            f"Enhanced LoL Rich Presence{'.exe' if platform.system() == 'Windows' else ''}",
        ]:
            func = shutil.copytree if os.path.isdir(path) else shutil.copy
            func(os.path.join("./", path), os.path.join("./backup", path))
        return

    def _gui_check_done(self) -> None:
        async def wrapper():
            while not self._finished:
                await asyncio.sleep(0)
            if not self._success:
                self._close_root()
            else:
                self.update_status_label.config(text="Update Completed")
                self.update_status_prog_bar.stop()
                self.update_status_prog_bar.config(value=100)
                self.update_status_finish_button.config(state="normal")

        self._loop.create_task(wrapper())

    def _cleanup(self) -> None:
        self._rmtree()
        self._finished = True

    def handle_update(self) -> None:
        self._t = threading.Thread(
            target=self._loop.run_until_complete,
            args=(self._handle_update(),)
        )
        self._t.start()

    async def _finish_early(self) -> None:
        await asyncio.sleep(1)
        self._success = False
        self._cleanup()

    async def _handle_update(self) -> None:
        _platform = "windows" if platform.system() == "Windows" else "macos"
        try:
            async with aiohttp.ClientSession() as cs:
                async with cs.get(f"https://custom-lolrpc.marwynn.me/update?version={self._version}&os={_platform}") as req:
                    data = await req.read()
                    content_type = req.content_type
            if content_type != "application/zip":
                await self._finish_early()
                return
        except Exception:
            await self._finish_early()
            return

        self._prep()

        async with aiopen("./tmp.zip", "wb") as file:
            await file.write(data)
        _zip = zipfile.ZipFile("./tmp.zip", "r")

        with zipfile.ZipFile("./tmp.zip", "r") as _zip:
            _zip.extractall("./tmp")
        os.remove("./tmp.zip")

        async with aiopen("./config.json", "r") as file:
            current_config = json.loads(await file.read())

        async with aiopen("./tmp/config.json", "r") as file:
            updated_config = json.loads(await file.read())

        if sorted(current_config.keys()) != sorted(updated_config.keys()):
            for key in updated_config.keys():
                if updated_config.get(key) and current_config.get(key):
                    updated_config[key] = current_config[key]
            async with aiopen("./tmp/config.json", "w") as file:
                await file.write(json.dumps(updated_config, indent=4))
        else:
            async with aiopen("./tmp/config.json", "w") as file:
                await file.write(json.dumps(current_config, indent=4))

        for path in os.listdir("./tmp"):
            if "updater" in path:
                continue
            if os.path.exists(os.path.join("./", path)):
                del_func = shutil.rmtree if os.path.isdir(path) else os.remove
                del_func(os.path.join("./", path))
            create_func = shutil.copytree if os.path.isdir(path) else shutil.move
            if os.path.isdir(path):
                os.mkdir(os.path.join("./", path))
            create_func(os.path.join("./tmp", path), os.path.join("./", path))

        self._cleanup()


def update(version: str) -> None:
    root = tk.Tk()
    root.title("Updater")

    window_width = 400
    window_height = 120
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    center_x = int(screen_width / 2 - window_width / 2)
    center_y = int(screen_height / 2 - window_height / 2)

    root.geometry(f"{window_width}x{window_height}+{center_x}+{center_y}")
    root.resizable(False, False)

    # Get icon for appropriate OS
    if platform.system() == "Windows":
        icon = os.path.abspath("./icons/icon.ico")
    else:
        icon = os.path.abspath("./icons/icon.icns")
    root.iconbitmap(icon)

    root.columnconfigure(0, weight=1)

    gui = UpdaterGUI(root, version)
    gui._show_window()

    subprocess.Popen(
        [os.path.abspath(
            f"./Enhanced LoL Rich Presence{'.exe'  if platform.system() == 'Windows' else ''}"
        ), "-n"]
    )
    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--version", action="store", required=True)
    args = parser.parse_args()
    update(args.version)
    sys.exit(0)
