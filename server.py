import os

from aiofiles import open as aiopen
from dotenv import load_dotenv
from sanic import Request, Sanic
from sanic.response import file_stream, json

load_dotenv(os.path.abspath("./.env"))

_HOST = os.getenv("HOST")
_ROUTE = os.getenv("ROUTE")
_API_KEY = os.getenv("RIOT_API_KEY")


app = Sanic(__name__)


def _is_most_updated_version(updated_version: str, client_version: str) -> float:
    for updated, client in zip(updated_version.split("."), client_version.split(".")):
        if int(updated) < int(client):
            break
        elif int(updated) > int(client):
            return False
    return True


@app.get(_ROUTE, host=_HOST)
async def fetch_api_key(request: Request) -> json:
    return json({
        "api_key": _API_KEY,
    })


@app.get("/update", host=_HOST)
async def check_for_update(request: Request):
    async with aiopen("./VERSION", "r") as file:
        current_version = await file.read()
    version = [request.args.get("version")][0]
    _platform = [request.args.get("os")][0]
    if not _is_most_updated_version(current_version, version):
        return await file_stream(
            f"./releases/{_platform}/LOLRPC-{current_version}.zip",
            filename=f"LOLRPC-{current_version}.zip",
            mime_type="application/zip"
        )
    return json({
        "status": 200,
    })


@app.get("/download/<platform>", host=_HOST)
async def download(request: Request, platform: str):
    platform = platform.lower()
    version = request.args.get("version")
    if version:
        current_version = version
    else:
        async with aiopen("./VERSION", "r") as file:
            current_version = await file.read()
    if platform in ["windows", "macos"]:
        return await file_stream(
            f"./releases/{platform}/LOLRPC-{current_version}.zip",
            filename=f"LOLRPC-{current_version}.zip",
            mime_type="application/zip"
        )
    else:
        return json({
            "status": 404,
            "message": "Unknown or unsupported operating system. Please use either \"windows\" or \"macos\""
        }, status=404)

if __name__ == "__main__":
    app.run("0.0.0.0", "3334")
