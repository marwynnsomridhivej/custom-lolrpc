import asyncio
import os

import aiohttp
from PIL import Image

from rpc import _CHAMPIONS_URL, _SQUARE_ASSET_BASE

_LANE_ICON_URLS = [
    ("top", "https://static.wikia.nocookie.net/leagueoflegends/images/e/ef/Top_icon.png/revision/latest/"),
    ("jungle", "https://static.wikia.nocookie.net/leagueoflegends/images/1/1b/Jungle_icon.png/revision/latest/"),
    ("middle", "https://static.wikia.nocookie.net/leagueoflegends/images/9/98/Middle_icon.png/revision/latest/"),
    ("support", "https://static.wikia.nocookie.net/leagueoflegends/images/e/e0/Support_icon.png/revision/latest/"),
    ("bottom", "https://static.wikia.nocookie.net/leagueoflegends/images/9/97/Bottom_icon.png/revision/latest/"),
]
_MASTERY_ICON_URLS = [
    ("1", "https://static.wikia.nocookie.net/leagueoflegends/images/d/d8/Champion_Mastery_Level_1_Flair.png/revision/latest/"),
    ("2", "https://static.wikia.nocookie.net/leagueoflegends/images/4/4d/Champion_Mastery_Level_2_Flair.png/revision/latest/"),
    ("3", "https://static.wikia.nocookie.net/leagueoflegends/images/e/e5/Champion_Mastery_Level_3_Flair.png/revision/latest/"),
    ("4", "https://static.wikia.nocookie.net/leagueoflegends/images/b/b6/Champion_Mastery_Level_4_Flair.png/revision/latest/"),
    ("5", "https://static.wikia.nocookie.net/leagueoflegends/images/9/96/Champion_Mastery_Level_5_Flair.png/revision/latest/"),
    ("6", "https://static.wikia.nocookie.net/leagueoflegends/images/b/be/Champion_Mastery_Level_6_Flair.png/revision/latest/"),
    ("7", "https://static.wikia.nocookie.net/leagueoflegends/images/7/7a/Champion_Mastery_Level_7_Flair.png/revision/latest/"),
]
_RANK_ICON_URLS = [
    ("iron", "https://static.wikia.nocookie.net/leagueoflegends/images/f/fe/Season_2022_-_Iron.png"),
    ("bronze", "https://static.wikia.nocookie.net/leagueoflegends/images/e/e9/Season_2022_-_Bronze.png"),
    ("silver", "https://static.wikia.nocookie.net/leagueoflegends/images/4/44/Season_2022_-_Silver.png"),
    ("gold", "https://static.wikia.nocookie.net/leagueoflegends/images/8/8d/Season_2022_-_Gold.png"),
    ("platinum", "https://static.wikia.nocookie.net/leagueoflegends/images/3/3b/Season_2022_-_Platinum.png"),
    ("diamond", "https://static.wikia.nocookie.net/leagueoflegends/images/e/ee/Season_2022_-_Diamond.png"),
    ("master", "https://static.wikia.nocookie.net/leagueoflegends/images/e/eb/Season_2022_-_Master.png"),
    ("grandmaster", "https://static.wikia.nocookie.net/leagueoflegends/images/f/fc/Season_2022_-_Grandmaster.png"),
    ("challenger", "https://static.wikia.nocookie.net/leagueoflegends/images/0/02/Season_2022_-_Challenger.png"),
]


async def fetch() -> None:
    cs = aiohttp.ClientSession()

    try:
        req = await cs.get(_CHAMPIONS_URL.format(locale="en_US"))
        _champs = await req.json()
        champ_names = [
            name for name in _champs["data"].keys()
        ]
        for champ in champ_names:
            req = await cs.get(_SQUARE_ASSET_BASE.format(champ))
            res = await req.read()
            if not os.path.exists(os.path.abspath("./assets/champions")):
                os.mkdir(os.path.abspath("./assets/champions"))
            with open(os.path.abspath(f"./assets/champions/{champ}.png"), "wb") as _img:
                _img.write(res)
            img = Image.open(os.path.abspath(f"./assets/champions/{champ}.png"))
            img = img.resize((512, 512), resample=Image.Resampling.LANCZOS)
            img.save(os.path.abspath(f"./assets/champions/{champ}.png"), format="PNG")
            img.close()
            print(f"Finished {champ}")
        for (lane, url) in _LANE_ICON_URLS:
            req = await cs.get(url)
            res = await req.read()
            if not os.path.exists(os.path.abspath("./assets/lanes")):
                os.mkdir(os.path.abspath("./assets/lanes"))
            with open(os.path.abspath(f"./assets/lanes/{lane}.png"), "wb") as _img:
                _img.write(res)
            img = Image.open(os.path.abspath(f"./assets/lanes/{lane}.png"))
            img = img.resize((512, 512), resample=Image.Resampling.LANCZOS)
            img.save(os.path.abspath(f"./assets/lanes/{lane}.png"), format="PNG")
            img.close()
            print(f"Finished {lane}")
        for (mastery, url) in _MASTERY_ICON_URLS:
            req = await cs.get(url)
            res = await req.read()
            if not os.path.exists(os.path.abspath("./assets/mastery")):
                os.mkdir(os.path.abspath("./assets/mastery"))
            with open(os.path.abspath(f"./assets/mastery/m{mastery}.png"), "wb") as _img:
                _img.write(res)
            img = Image.open(os.path.abspath(f"./assets/mastery/m{mastery}.png"))
            img = img.resize((512, 512), resample=Image.Resampling.LANCZOS)
            img.save(os.path.abspath(f"./assets/mastery/m{mastery}.png"), format="PNG")
            img.close()
            print(f"Finished m{mastery}")
        for (rank, url) in _RANK_ICON_URLS:
            req = await cs.get(url)
            res = await req.read()
            if not os.path.exists(os.path.abspath("./assets/rank")):
                os.mkdir(os.path.abspath("./assets/rank"))
            with open(os.path.abspath(f"./assets/rank/{rank}.png"), "wb") as _img:
                _img.write(res)
            print(f"Finished Rank: {rank.title()}")

    finally:
        await cs.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(fetch())
