import logging
import time
from multiprocessing import Queue

import lcu_driver
from lcu_driver.connection import Connection
from lcu_driver.utils import return_process


logger = logging.getLogger('lcu-driver')


class LCUConnector(lcu_driver.Connector):
    def __init__(self, *args, repeats: int = 5, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._repeats = repeats

    def start(self) -> None:
        """Starts the connector. This method should be overridden if different behavior is required.

        :rtype: None
        """
        try:
            def wrapper():
                for _ in range(self._repeats):
                    _proc = return_process(["LeagueClientUx.exe", "LeagueClientUx"])
                    if _proc is not None:
                        break
                    time.sleep(2**_)
                else:
                    self._repeat_flag = False
                    return
                connection = _proc
                self.create_connection(connection)
                self.loop.run_until_complete(self.connection.init())

                if self._repeat_flag and len(self.ws.registered_uris) > 0:
                    logger.debug('Repeat flag=True. Looking for new clients.')
                    wrapper()

            wrapper()
        except KeyboardInterrupt:
            logger.info('Event loop interrupted by keyboard')


connector = LCUConnector(repeats=5)


@connector.ready
async def ready(con: Connection) -> None:
    req = await con.request("GET", "/lol-lobby/v2/lobby")
    res = await req.json()
    if res.get("gameConfig"):
        payload = {
            "command": "queue_id",
            "data": res["gameConfig"]["queueId"],
        }
        connector.queue.put(payload)

    req = await con.request("GET", "/lol-summoner/v1/current-summoner/account-and-summoner-ids")
    res = await req.json()
    payload = {
        "command": "summoner_id",
        "data": res["summonerId"],
    }
    connector.queue.put(payload)
    return


@connector.ws.register("/lol-lobby/v2/lobby", event_types=("UPDATE",))
async def handle_spectate(con: Connection, event) -> None:
    payload = {
        "command": "queue_id",
        "data": event.data["gameConfig"]["queueId"],
    }
    connector.queue.put(payload)
    return


@connector.ws.register("/lol-champ-select/v1/current-champion", event_types=("CREATE", "UPDATE"))
async def handle_champ_select(con: Connection, event) -> None:
    payload = {
        "command": "champion",
        "data": event.data
    }
    connector.queue.put(payload)


@connector.close
async def stop(con: Connection) -> None:
    await connector.stop()


def start_connector(queue: Queue) -> None:
    connector.queue: Queue = queue
    connector.start()
