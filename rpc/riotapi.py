import asyncio
import os
from typing import List, NamedTuple

import aiohttp
from dotenv import load_dotenv

from .consts import _BASE_API_URL

load_dotenv(os.path.abspath("./.env"))


class ChampionMastery(NamedTuple):
    level: int
    points: int


class SummonerRank(NamedTuple):
    queue: str
    tier: str
    division: str
    lp: int
    wins: int
    losses: int
    wr: float

    def __eq__(self, other: "SummonerRank") -> bool:
        return self.tier == other.tier and self.division == other.division and self.lp == other.lp and self.wr == other.wr

    def __gt__(self, other: "SummonerRank") -> bool:
        tier_hierarchy = {
            "IRON": 0,
            "BRONZE": 1,
            "SILVER": 2,
            "GOLD": 3,
            "PLATINUM": 4,
            "DIAMOND": 5,
            "MASTER": 6,
            "GRANDMASTER": 7,
            "CHALLENGER": 8,
        }
        division_hierarchy = {
            "IV": 0,
            "III": 1,
            "II": 2,
            "I": 3,
        }
        if tier_hierarchy[self.tier] > tier_hierarchy[other.tier]:
            return True
        elif division_hierarchy[self.division] > division_hierarchy[other.division]:
            return True
        elif self.lp > other.lp:
            return True
        elif self.wr > other.wr:
            return True
        return False


class RiotAPIClient():
    def __init__(self, platform: str) -> None:
        self.platform = platform.lower()

        self._api_key = None
        self._ready = False
        self._encrypted_id: str = None
        self._loop = asyncio.get_event_loop()
        self._loop.create_task(self._init_rac())

    async def _init_rac(self) -> None:
        async with aiohttp.ClientSession() as cs:
            async with cs.get(os.getenv("API_KEY_URL")) as req:
                res = await req.json()
        self._api_key = res["api_key"]
        self._ready = True

    async def _wait_until_ready(self) -> None:
        while not self._ready:
            await asyncio.sleep(0)
        return

    async def _ensure_encrypted_id(self, summoner_name: str) -> None:
        await self._wait_until_ready()
        if self._encrypted_id is None:
            async with aiohttp.ClientSession(headers={
                "X-Riot-Token": self._api_key,
            }) as cs:
                async with cs.get(
                    _BASE_API_URL.format(
                        platform=self.platform,
                        endpoint=f"/lol/summoner/v4/summoners/by-name/{summoner_name}",
                    )
                ) as req:
                    _res = await req.json()
                    self._encrypted_id = _res["id"]
        return

    async def champion_mastery(self, summoner_name: str, champion_id: int) -> ChampionMastery:
        await self._ensure_encrypted_id(summoner_name)
        async with aiohttp.ClientSession(headers={
            "X-Riot-Token": self._api_key
        }) as cs:
            async with cs.get(
                _BASE_API_URL.format(
                    platform=self.platform,
                    endpoint=f"/lol/champion-mastery/v4/champion-masteries/by-summoner/{self._encrypted_id}/by-champion/{champion_id}"
                )
            ) as req:
                data = await req.json()
        return ChampionMastery(level=data.get("championLevel", 0), points=data.get("championPoints", 0))

    async def summoner_rank(self, summoner_name: str) -> List[SummonerRank]:
        await self._ensure_encrypted_id(summoner_name)
        async with aiohttp.ClientSession(headers={
            "X-Riot-Token": self._api_key,
        }) as cs:
            async with cs.get(
                _BASE_API_URL.format(
                    platform=self.platform,
                    endpoint=f"/lol/league/v4/entries/by-summoner/{self._encrypted_id}"
                )
            ) as req:
                data = await req.json()
        return [
            SummonerRank(
                queue="solo" if item["queueType"] == "RANKED_SOLO_5x5" else "flex",
                tier=item["tier"].lower(),
                division=item["rank"],
                lp=item["leaguePoints"],
                wins=item["wins"],
                losses=item["losses"],
                wr=round(item["wins"] * 100 / (item["wins"] + item["losses"]), 1),
            ) for item in data
        ]
