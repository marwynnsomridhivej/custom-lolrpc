import asyncio
import copy
import datetime
from multiprocessing import Queue
from typing import List

from pypresence import AioPresence

from .consts import (_CLIENT_ID, _OPGG_LIVE_GAME, Template,
                     convert_platform_for_url)
from .lcd import LiveClientData
from .riotapi import SummonerRank


class RPCButton():
    def __init__(self, data: dict) -> None:
        self.label: str = data["label"]
        self.url: str = data["url"]

    def to_dict(self) -> dict:
        return {
            "label": self.label,
            "url": self.url,
        }


class RPCPayload():
    def __init__(self, payload: dict) -> None:
        self.state: str = payload["state"]                  # In Game | KDA/CS/KP
        self.details: str = payload["details"]              # Map Name (Gamemode) (init using gamedata)
        self.start: int = None                              # Start time of game (init using gamedata)
        self.large_image: str = None      # Champ large image key
        self.large_text: str = None        # Champ name
        self.small_image: str = payload["small_image"]      # Lane or Mastery icon
        self.small_text: str = payload["small_text"]        # Lane or mastery level
        self.buttons: List[RPCButton] = [RPCButton(data) for data in payload["buttons"]]  # opgg live game | Custom

    def to_dict(self) -> dict:
        data = {
            "state": self.state,
            "details": self.details,
            "start": self.start,
            "large_image": self.large_image,
            "large_text": self.large_text,
            "small_image": self.small_image,
            "small_text": self.small_text,
            "buttons": [button.to_dict() for button in self.buttons],
            "instance": False,
        }
        if self.small_image == "":
            del data["small_image"]
            del data["small_text"]
        return data

    def copy(self) -> "RPCPayload":
        return copy.deepcopy(self)


class LoLPresence():
    def __init__(self, *, platform: str, data: RPCPayload, locale: str = "en_US",
                 gui_queue: Queue, lcu_queue: Queue) -> None:
        self.platform = platform

        self.rpc = AioPresence(_CLIENT_ID)
        self.lcd = LiveClientData(
            self.platform,
            locale=locale,
            lcu_queue=lcu_queue
        )

        self.use_payload = data
        self.reference_payload = self.use_payload.copy()

        self._name: str = None
        self._region: str = None
        self._loop = asyncio.get_event_loop()
        self._queue = gui_queue

    async def _connect(self) -> None:
        await self.lcd.wait_for_game_start()
        self._name = await self.lcd.active_player_name()
        self._region = convert_platform_for_url(self.platform).lower()
        await self.rpc.connect()
        return

    async def _update_rpc_data(self) -> None:
        self.lcd._check_queue_task.cancel()
        queue_info = await self.lcd.get_queue_info()
        skin_name = await self.lcd.get_current_skin_name(self._name)

        player_list = await self.lcd.player_list()
        current_player_data = [item for item in player_list if item["summonerName"] == self._name]
        lane = current_player_data[0]["position"]
        mastery = await self.lcd.get_current_champ_mastery(self._name)
        highest_rank = None
        rank_solo = None
        rank_flex = None
        rank_info = await self.lcd.get_current_summoner_rank(self._name)
        for rank in rank_info:
            if rank.queue == "solo":
                rank_solo = rank
            elif rank.queue == "flex":
                rank_flex = rank

            if highest_rank is None or rank > highest_rank:
                highest_rank = rank

        while True:
            game_stats = await self.lcd.game_stats()

            # Update state
            template = self.reference_payload.state
            score = await self.lcd.player_scores(self._name)
            self.use_payload.state = Template.replace(
                text=template,
                values={
                    Template.DEFAULT: "In Game",
                    Template.KDA: "/".join([
                        str(item) for item in [score["kills"], score["deaths"], score["assists"]]
                    ]),
                    Template.CS: score["creepScore"],
                    Template.CSPM: round(score["creepScore"] / max(game_stats["gameTime"] / 60, 0.01), 2),
                    Template.VISION_SCORE: round(score["wardScore"], 1),
                    Template.KP: f"{await self.lcd.get_current_kp(self._name)}%",
                },
            )

            # Update details
            template = self.reference_payload.details
            self.use_payload.details = Template.replace(
                text=template,
                values={
                    Template.DEFAULT: f"{queue_info.map_name} ({queue_info.queue_name})",
                    Template.MAP: queue_info.map_name,
                    Template.GAMEMODE: queue_info.queue_name,
                },
            )

            # Update start
            self.use_payload.start = int(datetime.datetime.now().timestamp() - game_stats["gameTime"])

            # Update large_image and large_text together
            self.use_payload.large_image = queue_info.champion.id.lower()
            self.use_payload.large_text = queue_info.champion.name
            if skin_name != "default":
                self.use_payload.large_text += f" ({skin_name})"

            # Update small_image and small_text together
            template = self.reference_payload.small_image
            self.use_payload.small_image = Template.replace(
                text=template,
                values={
                    Template.DEFAULT: "",
                    Template.LANE: lane.lower(),
                    Template.MASTERY: f"m{mastery.level}" if mastery.level else "",
                    Template.RANK: highest_rank.tier.lower() if highest_rank else "",
                    Template.RANK_SOLO: rank_solo.tier.lower() if rank_solo else "",
                    Template.RANK_FLEX: rank_flex.tier.lower() if rank_flex else "",
                },
            )
            self.use_payload.small_text = Template.replace(
                text=template,
                values={
                    Template.DEFAULT: "",
                    Template.LANE: lane.capitalize(),
                    Template.MASTERY: f"Mastery {mastery.level} | {mastery.points:,} points" if mastery.level else "",
                    Template.RANK: self._format_ranked_data(highest_rank),
                    Template.RANK_SOLO: self._format_ranked_data(rank_solo),
                    Template.RANK_FLEX: self._format_ranked_data(rank_flex),
                },
            )

            # Update buttons
            self.use_payload.buttons.clear()

            for button in self.reference_payload.buttons:
                if button.label == Template.LIVE_GAME:
                    label = "View Game Info"
                    url = _OPGG_LIVE_GAME.format(
                        region=self._region,
                        summoner_name=self._name,
                    )
                else:
                    label = button.label
                    url = button.url
                self.use_payload.buttons.append(
                    RPCButton({
                        "label": label,
                        "url": url,
                    })
                )

            try:
                await self.rpc.update(**self.use_payload.to_dict())
            except Exception as e:
                print(e)
            await asyncio.sleep(15)

    def _format_ranked_data(self, rank: SummonerRank) -> str:
        if rank is None:
            return ""
        return f"{rank.tier.capitalize()} {rank.division}: {rank.lp} LP | {rank.wins}W/{rank.losses}L ({rank.wr}% WR)"

    async def start(self) -> None:
        await self._connect()
        task = self._loop.create_task(self._update_rpc_data())
        task.add_done_callback(_handle_task)

        # Listen for when the game exits
        await self.lcd.wait_for_game_end()
        task.cancel()
        self.lcd.shutdown()
        self._queue.put_nowait({
            "command": "rpc_end"
        })


def _handle_task(task: asyncio.Task) -> None:
    try:
        task.result()
    except asyncio.CancelledError:
        pass
    return
