import asyncio
from typing import Dict, List

import aiohttp

_CLIENT_ID = "1006663582324371577"

_BASE_API_URL = "https://{platform}.api.riotgames.com{endpoint}"
_CHAMPIONS_URL = "https://ddragon.leagueoflegends.com/cdn/{version}/data/{locale}/champion.json"
_CHAMPION_DATA_URL = "https://ddragon.leagueoflegends.com/cdn/{version}/data/{locale}/champion/{id}.json"
_SQUARE_ASSET_BASE = "http://ddragon.leagueoflegends.com/cdn/{version}/img/champion/{}.png"
_VERSIONS_URL = "https://ddragon.leagueoflegends.com/api/versions.json"

_MAPS_URL = "https://static.developer.riotgames.com/docs/lol/maps.json"
_MAPS = {}
_GAMEMODES = {
    "PRACTICETOOL": "Practice Tool",
    "ARAM": "ARAM",
    "ULTBOOK": "Ultimate Spellbook",
    "NEXUSBLITZ": "Nexus Blitz",
    "ONEFORALL": "One for All",
    "URF": "Ultra Rapid Fire",
}

_PLATFORM_ROUTING_VALUES = {
    "BR1": "br1.api.riotgames.com",
    "EUN1": "eun1.api.riotgames.com",
    "EUW1": "euw1.api.riotgames.com",
    "JP1": "jp1.api.riotgames.com",
    "KR": "kr.api.riotgames.com",
    "LA1": "la1.api.riotgames.com",  # Latin America North
    "LA2": "la2.api.riotgames.com",  # Latin America South
    "NA1": "na1.api.riotgames.com",
    "OC1": "oc1.api.riotgames.com",
    "TR1": "tr1.api.riotgames.com",
    "RU": "ru.api.riotgames.com",
}

_PLATFORM_CONVERSION = {
    "Brazil (BR)": "BR1",
    "Europe Nordic & East (EUNE)": "EUN1",
    "Europe West (EUW)": "EUW1",
    "Japan (JP)": "JP1",
    "Republic of Korea (KR)": "KR",
    "Latin America North (LAN)": "LA1",
    "Latin America South (LAS)": "LA2",
    "North America (NA)": "NA1",
    "Oceania (OCE)": "OC1",
    "Turkey (TR)": "TR1",
    "Russia (RU)": "RU",
}

_LOCALES = {
    "cs_CZ": "Czech (Czech Republic)",                  # NOT DONE
    "el_GR": "Greek (Greece)",                          # NOT DONE
    "pl_PL": "Polish (Poland)",                         # NOT DONE
    "ro_RO": "Romanian (Romania)",                      # NOT DONE
    "hu_HU": "Hungarian (Hungary)",                     # NOT DONE
    "en_GB": "English (United Kingdom)",                # NOT DONE
    "de_DE": "German (Germany)",                        # NOT DONE
    "es_ES": "Spanish (Spain)",                         # NOT DONE
    "it_IT": "Italian (Italy)",                         # NOT DONE
    "fr_FR": "French (France)",                         # NOT DONE
    "ja_JP": "Japanese (Japan)",                        # IN PROGRESS
    "ko_KR": "Korean (Korea)",                          # NOT DONE
    "es_MX": "Spanish (Mexico)",                        # NOT DONE
    "es_AR": "Spanish (Argentina)",                     # NOT DONE
    "pt_BR": "Portuguese (Brazil)",                     # NOT DONE
    "en_US": "English (United States)",                 # IN PROGRESS
    "en_AU": "English (Australia)",                     # NOT DONE
    "ru_RU": "Russian (Russia)",                        # NOT DONE
    "tr_TR": "Turkish (Turkey)",                        # NOT DONE
    "ms_MY": "Malay (Malaysia)",                        # NOT DONE
    "en_PH": "English (Republic of the Philippines)",   # NOT DONE
    "en_SG": "English (Singapore)",                     # NOT DONE
    "th_TH": "Thai (Thailand)",                         # IN PROGRESS
    "vn_VN": "Vietnamese (Viet Nam)",                   # NOT DONE
    "id_ID": "Indonesian (Indonesia)",                  # NOT DONE
    "zh_MY": "Chinese (Malaysia)",                      # NOT DONE
    "zh_CN": "Chinese (China)",                         # NOT DONE
    "zh_TW": "Chinese (Taiwan)",                        # NOT DONE
}

_OPGG_LIVE_GAME = "https://{region}.op.gg/summoners/{region}/{summoner_name}/ingame"


async def _init() -> None:
    global _CHAMPIONS_URL, _CHAMPION_DATA_URL, _SQUARE_ASSET_BASE, _MAPS
    async with aiohttp.ClientSession() as cs:
        async with cs.get(_VERSIONS_URL) as req:
            res = await req.json()
        version = res[0]
        _CHAMPIONS_URL = _CHAMPIONS_URL.replace("{version}", version)
        _CHAMPION_DATA_URL = _CHAMPION_DATA_URL.replace("{version}", version)
        _SQUARE_ASSET_BASE = _SQUARE_ASSET_BASE.replace("{version}", version)

        async with cs.get(_MAPS_URL) as req:
            res = await req.json()
        for info in res:
            _MAPS[info["mapId"]] = info["mapName"]


def convert_platform_for_url(platform: str) -> str:
    if platform in ["BR1", "EUW1", "JP1", "KR", "NA1", "TR1", "RU"]:
        return platform.replace("1", "")
    elif platform in ["EUN1", "OC1"]:
        return platform.replace("1", "E")
    return "LAN" if platform == "LA1" else "LAS"


class Template():
    DEFAULT: str = "{default}"
    CHAMPION: str = "{champion}"
    SKIN: str = "{skin}"
    KDA: str = "{kda}"
    CS: str = "{cs}"
    CSPM: str = "{cspm}"
    VISION_SCORE: str = "{vision}"
    KP: str = "{kp}"
    MAP: str = "{map}"
    GAMEMODE: str = "{gamemode}"
    LANE: str = "{lane}"
    MASTERY: str = "{mastery}"
    RANK: str = "{rank}"
    RANK_SOLO: str = "{rank_solo}"
    RANK_FLEX: str = "{rank_flex}"
    LIVE_GAME: str = "{livegame}"
    
    # Provide prebuilt specifics
    STATE: List[str] = [KDA, CS, CSPM, VISION_SCORE, KP]
    DETAILS: List[str] = [MAP, GAMEMODE]

    @classmethod
    def replace(cls, text: str, values: Dict[str, str]) -> str:
        if text == cls.DEFAULT:
            return values[cls.DEFAULT]
        for (key, value) in values.items():
            text = text.replace(key, str(value))
        return text

    @classmethod
    def sample(cls, text: str, specifics: List[str] = None) -> str:
        options = {
            Template.KDA: "3/1/4",
            Template.CS: "153",
            Template.CSPM: "8.69",
            Template.VISION_SCORE: "20.2",
            Template.KP: "63.6%",
            Template.MAP: "Summoner's Rift",
            Template.GAMEMODE: "Ranked Solo",
            Template.LANE: ("middle", "Middle"),
            Template.MASTERY: ("7", "Mastery 7 | 32,968 points"),
            Template.RANK: ("diamond", "Diamond I: 52 LP | 182W/143L (56.0% WR)"),
            Template.RANK_SOLO: ("diamond", "Diamond I: 52 LP | 182W/143L (56.0% WR)"),
            Template.RANK_FLEX: ("platinum", "Platinum III: 98 LP | 20W/17L (56.0% WR)"),
        }
        if specifics is not None:
            options = {key: value for key, value in options.items() if key in specifics}
        return cls.replace(text, options)


loop = asyncio.get_event_loop()
loop.run_until_complete(_init())
