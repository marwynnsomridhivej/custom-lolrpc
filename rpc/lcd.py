import asyncio
import multiprocessing
from queue import Empty
from typing import List, NamedTuple, Union

import aiohttp
from aiohttp.client_exceptions import (ClientConnectorError,
                                       ServerDisconnectedError)

from .consts import _CHAMPION_DATA_URL, _CHAMPIONS_URL, _GAMEMODES, _MAPS
from .riotapi import ChampionMastery, RiotAPIClient, SummonerRank

_BASE = "https://127.0.0.1:2999/liveclientdata/{}"
_CHAMPIONS = {}
_QUEUES_URL = "https://static.developer.riotgames.com/docs/lol/queues.json"
_QUEUES = {
    -1: "Summoner's Rift"
}
_QUEUE_ID_CONVERSION = {
    -1: "Practice Tool",
    0: "Custom",
    76: "Ultra Rapid Fire",
    400: "Normal Draft",
    420: "Ranked Solo/Duo",
    430: "Normal Blind",
    440: "Ranked Flex",
    450: "ARAM",
    700: "Clash",
    830: "Intro Co-op vs. AI",
    840: "Beginner Co-op vs. AI",
    850: "Intermediate Co-op vs. AI",
    900: "ARURF",
    1020: "One for All",
    1300: "Nexus Blitz",
    1400: "Ultimate Spellbook",
    1900: "Ultra Rapid Fire",
    2000: "Tutorial I",
    2010: "Tutorial II",
    2020: "Tutorial III",
}


def _handle_task(task: asyncio.Task) -> None:
    try:
        task.result()
    except asyncio.CancelledError:
        pass
    return


class ChampionSkin(NamedTuple):
    id: str
    num: int
    name: str
    chromas: bool


class Champion(NamedTuple):
    id: str
    name: str
    key: int
    skins: List[ChampionSkin]


class QueueInfo(NamedTuple):
    map_name: str
    queue_name: str
    champion: Champion


class LiveClientData():
    def __init__(self, platform: str, locale: str = "en_US",
                 lcu_queue: multiprocessing.Queue = None) -> None:
        self._loop = asyncio.get_event_loop()

        self._queue_id: int = None
        self._current_champ: Champion = None
        self._summoner_id: int = None
        self._platform = platform
        self._locale = locale
        self._rac: RiotAPIClient = RiotAPIClient(self._platform)

        self._lcu_queue = lcu_queue
        self._ready = False
        self._startup()

    async def _request(self, endpoint: str) -> dict:
        async with aiohttp.ClientSession() as cs:
            async with cs.get(_BASE.format(endpoint), ssl=False) as res:
                resp = await res.json()
        return resp

    async def all_game_data(self) -> dict:
        return await self._request("allgamedata")

    async def active_player(self) -> dict:
        return await self._request("activeplayer")

    async def active_player_name(self) -> Union[str, None]:
        name = await self._request("activeplayername")
        return name if isinstance(name, str) and name else None

    async def active_player_abilities(self) -> dict:
        return await self._request("activeplayerabilities")

    async def active_player_runes(self) -> dict:
        return await self._request("activeplayerrunes")

    async def player_list(self) -> List[dict]:
        return await self._request("playerlist")

    async def player_scores(self, summoner_name: str) -> dict:
        return await self._request(f"playerscores?summonerName={summoner_name}")

    async def player_summoner_spells(self, summoner_name: str) -> dict:
        return await self._request(f"playersummonerspells?summonerName={summoner_name}")

    async def player_main_runes(self, summoner_name: str) -> dict:
        return await self._request(f"playermainrunes?summonerName={summoner_name}")

    async def player_items(self, summoner_name: str) -> List[dict]:
        return await self._request(f"playeritems?summonerName={summoner_name}")

    async def event_data(self) -> dict:
        return await self._request("eventdata")

    async def game_stats(self) -> dict:
        return await self._request("gamestats")

    async def get_queue_info(self) -> QueueInfo:
        if self._queue_id is None:
            game_stats = await self.game_stats()
            map_name = _MAPS.get(game_stats["mapNumber"])
            queue_name = _GAMEMODES.get(
                game_stats["gameMode"],
                str(game_stats["gameMode"]).capitalize()
            )
        else:
            map_name = _QUEUES.get(self._queue_id)
            queue_name = _QUEUE_ID_CONVERSION.get(self._queue_id)
        if self._current_champ is None:
            self._current_champ = await self._get_current_champ()
        return QueueInfo(
            map_name=map_name,
            queue_name=queue_name,
            champion=self._current_champ,
        )

    async def get_current_skin_name(self, summoner_name: str) -> str:
        players = await self.player_list()
        for player in players:
            if player["summonerName"] == summoner_name:
                self_info = player
                break
        else:
            return self._current_champ.name

        skin_id = self_info["skinID"]
        for skin in self._current_champ.skins:
            if skin.num == skin_id:
                skin_data = skin
                break
        else:
            return self._current_champ.name

        return skin_data.name

    async def get_current_champ_mastery(self, summoner_name: str) -> ChampionMastery:
        return await self._rac.champion_mastery(summoner_name, self._current_champ.key)

    async def get_current_summoner_rank(self, summoner_name: str) -> List[SummonerRank]:
        return await self._rac.summoner_rank(summoner_name)

    async def get_current_kp(self, summoner_name: str) -> float:
        total_kills = my_kills = my_assists = 0
        players = await self.player_list()
        my_team = [player["team"] for player in players if player["summonerName"] == summoner_name][0]
        for player in players:
            if player["team"] == my_team:
                stats = player["scores"]
                total_kills += stats["kills"]
                if player["summonerName"] == summoner_name:
                    my_kills = stats["kills"]
                    my_assists = stats["assists"]
        kp = (my_kills + my_assists) / total_kills if total_kills > 0 else 0
        return round(kp * 100, 1) if kp > 0 else 0

    async def wait_for_game_start(self) -> None:
        while True:
            try:
                name = await self.active_player_name()
            except Exception:
                continue
            if name is not None:
                break
            await asyncio.sleep(0)
        return

    async def wait_for_game_end(self) -> None:
        while True:
            try:
                await self.active_player_name()
            except (ClientConnectorError, ServerDisconnectedError):
                break
            await asyncio.sleep(0)
        return

    async def _init_consts(self) -> None:
        async with aiohttp.ClientSession() as cs:
            async with cs.get(_QUEUES_URL) as req:
                res = await req.json()
                for data in res:
                    _QUEUES[data["queueId"]] = data["map"]
            async with cs.get(_CHAMPIONS_URL.format(locale=self._locale)) as req:
                res = await req.json()

        async def _task(data):
            async with aiohttp.ClientSession() as cs:
                async with cs.get(_CHAMPION_DATA_URL.format(locale=self._locale, id=data["id"])) as champ_req:
                    champ_res = await champ_req.json()

            champ_id = data["id"]
            champ_name = data["name"]
            champ_key = int(data["key"])
            champ_skins_info = champ_res["data"][champ_id]["skins"]
            skins = [ChampionSkin(**skin) for skin in champ_skins_info]
            champion = Champion(
                id=champ_id,
                name=champ_name,
                key=champ_key,
                skins=skins,
            )
            _CHAMPIONS[int(data["key"])] = champion

        tasks = []
        for (_, data) in res["data"].items():
            tasks.append(self._loop.create_task(_task(data)))
        await asyncio.gather(*tasks)
        self._ready = True

    async def _get_current_champ(self) -> Champion:
        while not self._ready:
            await asyncio.sleep(1)
        players = await self.player_list()
        name = await self.active_player_name()
        for player in players:
            if player["summonerName"] == name:
                active_player_data = player
        raw_champion_name = str(active_player_data["rawChampionName"])
        champion_name = (raw_champion_name.split("_"))[-1]
        for (key, champion) in _CHAMPIONS.items():
            if champion.id == champion_name:
                break
        else:
            raise ValueError(f"Current champion name: {champion_name} could not be found")
        return champion

    def _startup(self) -> None:
        self._loop.create_task(self._init_consts()).add_done_callback(_handle_task)
        self._check_queue_task = self._loop.create_task(self._check_lcu_queue())
        self._check_queue_task.add_done_callback(_handle_task)

    async def _check_lcu_queue(self) -> None:
        while True:
            try:
                payload = self._lcu_queue.get_nowait()
            except Empty:
                await asyncio.sleep(1)
            else:
                if payload["command"] == "queue_id":
                    self._queue_id = payload["data"]
                elif payload["command"] == "champion":
                    self._current_champ = _CHAMPIONS.get(payload["data"])
                elif payload["command"] == "summoner_id":
                    self._summoner_id = payload["data"]

    def shutdown(self) -> None:
        if not self._check_queue_task.cancelled():
            self._check_queue_task.cancel()
