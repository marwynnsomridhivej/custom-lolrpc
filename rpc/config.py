import json
import os
from tkinter import StringVar, Tk
from typing import Any, List

from .rpc import RPCButton

_DEFAULT = {
    "state": "{default}",
    "details": "{default}",
    "small_image": "{default}",
    "small_text": "{default}",
    "buttons": [],

    "platform": "NA1",
    "locale": "en_US",
}


class Config():
    def __init__(self, root: Tk, **data):
        self.state: StringVar = StringVar(root, value=data["state"])
        self.details: StringVar = StringVar(root, value=data["details"])
        self.small_image: StringVar = StringVar(root, value=data["small_image"])
        self.small_text: StringVar = StringVar(root, value=data["small_text"])
        self.buttons: List[RPCButton] = [RPCButton(button) for button in data["buttons"]]

        self.platform: StringVar = StringVar(root, value=data["platform"])
        self.locale: StringVar = StringVar(root, value=data.get("locale", "en_US"))

    def edit(self, key: str, value: Any) -> None:
        _var = getattr(self, key)
        _var.set(value)
        return

    def to_dict(self) -> dict:
        return {
            "state": self.state.get(),
            "details": self.details.get(),
            "small_image": self.small_image.get(),
            "small_text": self.small_text.get(),
            "buttons": [
                button.to_dict() for button in self.buttons
            ],

            "platform": self.platform.get(),
            "locale": self.locale.get(),
        }

    def save(self) -> None:
        with open("./config.json", "w") as file:
            data = json.dumps(self.to_dict(), indent=4)
            file.write(data)
        return


def init_config_file() -> None:
    if not os.path.exists("./config.json"):
        with open("./config.json", "w") as file:
            json.dump(_DEFAULT, file, indent=4)
    return


def get_config(root: Tk) -> Config:
    init_config_file()
    with open("./config.json", "r") as file:
        config = Config(root, **(json.loads(file.read())))
    return config
