import json
import re

from bs4 import BeautifulSoup

data = {}
URL_RX = re.compile("https:\/\/cdn.discordapp.com\/app-assets\/1006663582324371577\/[\d]{19}.png")

with open("./Discord Developer Portal.html", "r") as file:
    bs = BeautifulSoup(file.read(), "lxml")

elements = bs.find_all(class_="content-19OCyi")
for element in elements:
    url_element = element.find(class_="image-1j6qbZ")
    url_search = URL_RX.search(url_element["style"])
    url = url_search.group(0)

    name_element = element.find(class_="nameWrapper-1TC8j1")
    name = name_element.text

    data[name] = url


with open("./asset_ids", "w") as file:
    file.write(json.dumps(data, indent=4))
