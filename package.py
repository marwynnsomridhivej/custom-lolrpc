import os
import platform
import shutil

import PyInstaller.__main__ as PI

from gui import VERSION

# Generate .exe
PI.run([
    "./gui.py",
    "--onefile",
    f"-i=./icons/icon.{'icns' if platform.system() == 'Darwin' else 'ico'}",
    "-n=Enhanced LoL Rich Presence",
    "-w",
])

PI.run([
    "./updater.py",
    "--onefile",
    f"-i=./icons/icon.{'icns' if platform.system() == 'Darwin' else 'ico'}",
    "-n=updater",
    "-w",
])


# Copy relevant files over to ./dist
target = os.path.abspath("./dist")
sources = [
    "/assets/champions/Yone.png",
    "/assets/lanes/middle.png",
    "/assets/mastery/m7.png",
    "/assets/rank/diamond.png",
    "/assets/rank/platinum.png",
]

shutil.copy("./sample-config.json", target)

if not os.path.exists("./dist/assets"):
    os.mkdir("./dist/assets")
if not os.path.exists("./dist/assets/champions"):
    os.mkdir("./dist/assets/champions")
if not os.path.exists("./dist/assets/lanes"):
    os.mkdir("./dist/assets/lanes")
if not os.path.exists("./dist/assets/mastery"):
    os.mkdir("./dist/assets/mastery")
if not os.path.exists("./dist/assets/rank"):
    os.mkdir("./dist/assets/rank")

for source in sources:
    source_path = f".{source}"
    path = target + source
    print(source_path, path)
    shutil.copy(source_path, path)

shutil.copytree("./icons", os.path.join(target, "icons"), dirs_exist_ok=True)
shutil.copytree("./lang", os.path.join(target, "lang"), dirs_exist_ok=True)

shutil.move(
    "./dist/sample-config.json",
    "./dist/config.json",
)

shutil.make_archive(f"LOLRPC-{VERSION}", "zip", os.path.abspath("./dist"))

if not os.path.exists("./releases"):
    os.mkdir("./releases")

shutil.move(
    f"./LOLRPC-{VERSION}.zip",
    f"./releases/LOLRPC-{VERSION}.zip"
)

with open("./VERSION", "w") as file:
    file.write(VERSION)
